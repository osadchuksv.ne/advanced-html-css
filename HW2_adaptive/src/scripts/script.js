// const burgerMenuBtn = document.querySelector(".header__burger-menu-btn img");
const burgerMenuBtn = document.querySelector(".burger-btn-img");
const burgerMenu = document.querySelector(".header__nav-menu");


window.addEventListener('resize', () => {
    if (window.matchMedia("(min-width: 769px)").matches) {
        burgerMenu.style.display = "flex";
    } else if (window.matchMedia("(max-width: 768px)").matches) {
        burgerMenuBtn.setAttribute("src", "./images/icon_burger-menu.svg");
        burgerMenu.style.display = "none";

    }
    
});

burgerMenuBtn.addEventListener("click", () => {

    if(burgerMenuBtn.getAttribute("src") === "./images/icon_burger-menu.svg") {
        burgerMenuBtn.setAttribute("src", "./images/icon_menu-close.svg");
        burgerMenu.style.display = "flex";
    } else if (burgerMenuBtn.getAttribute("src") != "./images/icon_burger-menu.svg") {
        burgerMenuBtn.setAttribute("src", "./images/icon_burger-menu.svg");
        burgerMenu.style.display = "none";
    }

})

